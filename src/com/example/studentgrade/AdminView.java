package com.example.studentgrade;


import com.example.studentgrade.R;

import android.app.TabActivity;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.GridView;
import android.widget.TabHost;
import android.widget.TextView;

public class AdminView extends TabActivity {
	
	DatabaseHelper dbHelper;
	GridView grid;
	TextView txtTest;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.admin_view);
		SetupTabs();
		
	}
	
	@Override
    public boolean onCreateOptionsMenu(Menu menu)
    {
    	return true;
    }
    
    
    
    public boolean onOptionsItemSelected(MenuItem item)
    {
    	switch (item.getItemId())
    	{
    	//Add student
    	case 1:
    		Intent addIntent=new Intent(this,AddStudent.class);
    		startActivity(addIntent);
    		break;
    	}
    	super.onOptionsItemSelected(item);
    	return false;
    }
    
    void SetupTabs()
    {

    	TabHost host=getTabHost();

        TabHost.TabSpec spec=host.newTabSpec("tag1");
        Intent in1=new Intent(this, AddStudent.class);
        spec.setIndicator("Add Student dept.");
        spec.setContent(in1);
        
        
        
        TabHost.TabSpec spec2=host.newTabSpec("tag2");
        Intent in2=new Intent(this, GridList.class);
        
        spec2.setIndicator("View Student dept.");
        spec2.setContent(in2);
        
        host.addTab(spec);
        host.addTab(spec2);
        
       
    }
}