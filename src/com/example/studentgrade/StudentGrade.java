package com.example.studentgrade;

import com.example.studentgrade.R;

import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.view.Menu;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class StudentGrade extends Activity {

	private TextView txt_username;
	private TextView txt_password;
	private Button btn_login;
	private Button btn_cancel;
	private EditText et_username;
	private EditText et_password;
	private Button btn_student;
	private Button btn_admin;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_student_grade);
		
		txt_username = (TextView)findViewById(R.id.txt_username);
		txt_password = (TextView)findViewById(R.id.txt_password);
		btn_login 	 = (Button)findViewById(R.id.btn_login);
		btn_cancel 	 = (Button)findViewById(R.id.btn_cancel);
		et_username  = (EditText)findViewById(R.id.edtxt_username);
		et_password  = (EditText)findViewById(R.id.edtxt_password);
		btn_student  = (Button)findViewById(R.id.btn_student);
		btn_admin 	 = (Button)findViewById(R.id.btn_admin);
		
		btn_admin.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
            	// TODO Auto-generated method stub
            	txt_username.setVisibility(View.VISIBLE);
            	txt_password.setVisibility(View.VISIBLE);
            	et_username.setVisibility(View.VISIBLE);
            	et_password.setVisibility(View.VISIBLE);
            	btn_login.setVisibility(View.VISIBLE);
            	btn_cancel.setVisibility(View.VISIBLE);
            }
        });
		
		btn_student.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent studentView = new Intent(getApplicationContext(),StudentView.class);
				startActivity(studentView);
			}
		});
		
		btn_login.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if(et_username.getText().toString().equals("admin") && et_password.getText().toString().equals("1234"))
				{
					Intent AdminView = new Intent(getApplicationContext(),AdminView.class);
					startActivity(AdminView);
				}else
				{
					et_password.setText("");
					et_username.setText("");
				}
				
			}
		});
		
		btn_cancel.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				et_password.setText("");
				et_username.setText("");
			}
		});
		
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.student_grade, menu);
		return true;
	}

}
