package com.example.studentgrade;

import android.content.Context;

public class Student {
	
	int _id;
	String _name;
	int _age;
	int _dept;
	
	public Student(String Name,int Age,int Dept)
	{
		
		this._name=Name;
		this._age=Age;//student id
		this._dept=Dept;
	}
	
	public Student(String Name,int Age)
	{
		this._name=Name;
		this._age=Age;
	}
	
	public int getID()
	{
		return this._id;
	}
	public void SetID(int ID)
	{
		this._id=ID;
	}
	
	public String getName()
	{
		return this._name;
	}
	
	public int getAge()//studen id
	{
		return this._age;
	}
	
	public void setName(String Name)
	{
		this._name=Name;
	}
	public void setAge(int Age)
	{
		this._age=Age;
	}
	
	
	
	public void setDept(int Dept)
	{
		this._dept=Dept;
	}
	
	public String getDeptName(Context con, int Dept)
	{
		return new DatabaseHelper(con).GetDept(Dept);
	}
	public int getDept()
	{
		return this._dept;
	}
}
